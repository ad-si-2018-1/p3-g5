## Alunos

- Daniel Cavalcante
- Guilherme Paniago
- Guilherme Quirino de Melo - Dev
- Gilmar Alves Bernardes Júnior - Dev

## Projeto 3

Tendo como base o projeto 2.
Foi acrescentado o middleware RabbitMQ que realiza o gerenciamento de filas para troca de mensagens
As filas de mensagens são identificadas pelo nome do canal.
Os comandos são armazenados em filas com nome de 'commands'.
