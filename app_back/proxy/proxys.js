let irc = require('irc');
const proxies = {}; // mapa de proxys
let proxy_id = 0;
let clientCache = [];

module.exports = {
  proxy: (id, servidor, nick, canal, socket) => {
    let irc_client = new irc.Client(servidor, nick, { channels: [canal] });
    require('./proxyListenners')(irc_client, socket);
    proxies[id] = { ws: socket, irc_client: irc_client, clientCache };
    return proxies[id];
  },

  incrementIdProxy: () => {
    this.proxy_id++;
    return this.proxy_id;
  },

  getClient: proxy_id => {
    return proxies[proxy_id].irc_client;
  },

  getProxy: proxy_id => {
    return proxies[proxy_id];
  },

  getProxyId: () => {
    return proxy_id;
  }
};
