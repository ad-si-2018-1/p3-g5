const app = require('express')();
const server = require('http').Server(app);
const socketio = require('socket.io')(server);
const { port } = require('./app_back/config/config');
const amqp = require('./app_back/middleware/rabbitMQ');
require('./app_back/routes/routes')(app);

amqp.connect();

server.listen(port, function() {
  console.log('Example app listening on port %s!', port);
});

module.exports = socketio;
