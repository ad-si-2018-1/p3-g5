## Criando um Bot no telegram

Bot do telegram com RabbitMQ para gerenciamento para troca de mensagens entre o bot e o telegram

Tutorial para criar bot no telegram

1.  No telegram pesquise por `@BotFather`
2.  Digite o comando /start
3.  Digite o comando /newbot
4.  Forneça o nome do bot
5.  Forneça o username do bot
6.  O `@Botfather` irá fornecer o Tokem do bot

Se o bot não estiver recebendo as mensagens:

1.  Desative o modo de privacidade usando o `@Botfather`. ( /setprivacy )
2.  Retire o Bot da sala
3.  Convide o Bot novamente.

## Iniciar Bot

1.  Iniciar o terminal
2.  Acessar a pasta raiz do projeto
3.  Executar o comando "npm run bot-init"

## Bot Telegram

- Username do bot: @dev_adsi_g5_bot
- Grupo para testes com o bot:
- adsi-g5 : https://t.me/joinchat/FyAnKRBrZkd-BBEaBKRjAw

## Eventos escutados pelo bot

- Novo membro no grupo: 'new_chat_members'
- Membro deixa grupo: 'left_chat_members'
- Mensagens enviadas pelos usuários no grupo: 'messages'
