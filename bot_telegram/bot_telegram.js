const TelegramBot = require(`node-telegram-bot-api`);
const { TokemBot } = require('../app_back/config/config');
const bot = new TelegramBot(TokemBot, { polling: true });
const amqp = require('../app_back/middleware/rabbitMQ');

amqp.connect();

function enviarMsgTelegram(id) {
  amqp.receberDaFila(fila, msg => {
    let mensagem;
    if (msg.join_chat || msg.left_chat) {
      mensagem = 'Mensagem da fila AMQP: ' + fila + '\n' + msg.msg;
    }
    if (msg.text) {
      mensagem =
        'Mensagem da fila AMQP: ' +
        fila +
        '\n' +
        'de: ' +
        msg.from.first_name +
        ' ' +
        msg.from.last_name +
        '\n' +
        msg.text;
    }
    bot.sendMessage(id, mensagem);
  });
}

//quando o bot recebe alguma mensagem
bot.on('message', msg => {
  fila = msg.chat.id.toString();
  amqp.enviarParaFila(fila, msg);
  enviarMsgTelegram(msg.chat.id);
});

//quando alguém entra no grupo em que o bot está
bot.on('new_chat_members', msg => {
  fila = msg.chat.id.toString();
  amqp.enviarParaFila(fila, {
    join_chat: true,
    msg: `Olá ${msg.from.first_name}, Bem vindo ao grupo!`
  });
  enviarMsgTelegram(msg.chat.id);
});

//quando alguém sai do grupo que o bot está
bot.on('left_chat_member', msg => {
  fila = msg.chat.id.toString();
  amqp.enviarParaFila(fila, {
    left_chat: true,
    msg: `Por que será que o ${msg.from.first_name}, saiu do grupo?`
  });
  enviarMsgTelegram(msg.chat.id);
});

//mensagens emitidas pelo telegram
//http://python-telegram-bot.readthedocs.io/en/stable/telegram.message.html
